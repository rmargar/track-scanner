import cv2
import pytesseract
import numpy as np
import os

class ImageParser(object):
    
    """
    Object that parses the data from an image containing
    several tracks and writes it in dictionary style
    """

    def __init__(self,filepath,custom_config=None):
        self.img = cv2.imread(filepath)
        self.data = self._parse_image(custom_config)

    def _image_to_string(self,custom_config):
        return pytesseract.image_to_string(self.img,custom_config)
         
    def _parse_image(self,custom_config):
        """
        Transforms object to list
        """
        st = self._image_to_string(custom_config)
        st_list = list(filter(None,st.split('\n')))
        data = dict.fromkeys(['artist','track','label'])
        data['artist'] = []
        data['track'] = []
        data['label'] = []
        for s in st_list:
            try:
                l = s.split(',')
                assert len(l) == 3
                data['artist'].append(l[0])
                data['track'].append(l[1].replace('«','').replace('»',''))
                data['label'].append(l[2])
            except (AssertionError, RuntimeError):
                pass
        return data
        
    def plot_boxes(self):
        h, w = self.img.shape
        boxes = pytesseract.image_to_boxes(img) 
        for b in boxes.splitlines():
            b = b.split(' ')
            img = cv2.rectangle(img, (int(b[1]), h - int(b[2])), (int(b[3]), h - int(b[4])), (0, 255, 0), 2)
        cv2.imshow('img', self.img)
        cv2.waitKey(0)

    # get grayscale image
    def get_grayscale(self):
        return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # noise removal
    def remove_noise(self):
        return cv2.medianBlur(image,5)
    
    #thresholding
    def thresholding(self):
        return cv2.threshold(self.img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    #dilation
    def dilate(self):
        kernel = np.ones((5,5),np.uint8)
        return cv2.dilate(self.img, kernel, iterations = 1)
        
    #erosion
    def erode(self):
        kernel = np.ones((5,5),np.uint8)
        return cv2.erode(image, kernel, iterations = 1)

    #opening - erosion followed by dilation
    def opening(self):
        kernel = np.ones((5,5),np.uint8)
        return cv2.morphologyEx(self.img, cv2.MORPH_OPEN, kernel)

    #canny edge detection
    def _canny(self):
        return cv2.Canny(self.img, 100, 200)

    #skew correction
    def _deskew(self):
        coords = np.column_stack(np.where(image > 0))
        angle = cv2.minAreaRect(coords)[-1]
        if angle < -45:
            angle = -(90 + angle)
        else:
            angle = -angle
        (h, w) = self.img.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
        return rotated

    #template matching
    def _match_template(image, template):
        return cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED) 

if __name__ == "__main__":
    imgpath = os.path.abspath('./test_images/test2.jpg')
     # Adding custom options
    # custom_config = r'--oem 3 --psm 6'
    pg = ImageParser(imgpath)

