import os
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import sys


class Artist(object):
    def __init__(self,qname,spotify_session):
        self.url = self._get_url(qname,spotify_session)
        self.name = qname

    def _get_url(self,qname,spotify_session):
        results = spotify_session.search(q='artist:' + qname, type='artist')
        items = results['artists']['items']
        for i in items:
            print(i['name'])
        return items[0]['uri']

if __name__ == "__main__":
    artist1 = Artist('radiohead',spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials()))
    print(artist1.url)